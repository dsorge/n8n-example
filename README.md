<!-- PROJECT LOGO -->
<br />
<p align="center">
  <h3 align="center">n8n.io - Level 1: Beginners' course</h3>

  <p align="center">
    <br />
    <br />
    <a href="https://docs.n8n.io/courses/level-one/">Explore the docs</a>
  </p>
</p>

<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
  </ol>
</details>

<!-- GETTING STARTED -->
## Getting Started

Also have al look at the offical n8n docker image [instructions](https://github.com/n8n-io/n8n/blob/master/docker/images/n8n/README.md).

### Prerequisites

You need to install [Docker](https://www.docker.com/get-started).

### Start

1. Clone this repository
2. Open a terminal and and switch to the cloned project directory
3. Maybe change some settings in the file .env
4. Run 
  ``` docker-compose -f "docker-compose.yml" up -d --build ```
5. Automate some workflows ;-)
